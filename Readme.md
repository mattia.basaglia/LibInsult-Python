LibInsult Python
================

This repository contains the python bindings for libinsult.


License
-------

GPLv3+, see COPYING


Sources
-------

See http://insult.mattbas.org


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
